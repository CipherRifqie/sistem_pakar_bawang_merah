<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();
        $this->load->database();
		$this->load->model('crud');
		$this->load->helper('url');
	}

	public function index()
	{
		$this->load->view('front/plugin/header');
		$this->load->view('front/index');
		$this->load->view('front/plugin/footer');
	}
	public function login()
	{
		$this->load->view('front/plugin/header');
		$this->load->view('front/login');
		$this->load->view('front/plugin/footer');
	}

	public function register()
	{
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$email = $this->input->post('email');
		$username = $this->input->post('user');
		$password = md5($this->input->post('pass'));

		if(!empty($email)){
			$email = $this->input->post('email');
		}else{
			$email = "email_bawang@gmail.com";
		}
		$where = array('username' => $username);
		$cek_username = $this->crud->Get_user('tb_user',$where)->num_rows();

		if ($cek_username > 0) {
			# code...
			$this->session->set_userdata('username', 'username sudah terdaftar');
			redirect(base_url());
		}else{
				$data = array(
								'nama' => $nama,
								'alamat' => $alamat,
								'email'  => $email,
								'username' => $username,
								'password' => $password,
								'level' => 'user'
							 );
				$this->crud->Insert('tb_user', $data);
				$data_session = array(
										'register' => 'sukses',
										'nama' => $username,
										'status' => 'login'
									 );
				$this->session->set_userdata($data_session);
				redirect('/user');
		}

	}
	public function login_aksi()
	{
		$username = $this->input->post('user');
		$password = md5($this->input->post('pass'));
		$data = array(
					'username' => $username,
					'password' => $password
			);
		$cek = $this->crud->Get_user('tb_user', $data)->num_rows();

		if($cek == 1){
			$cek_level = $this->crud->user_where('tb_user', $data);
			$level = $cek_level[0]["level"];
			if ($level == 'admin') {
				# code...
				$data_session = array(
				'nama' => $username,
				'status' => "login"
				);
 
				$this->session->set_userdata($data_session);
				redirect(base_url("admin/dashboard"));
			}elseif ($level == 'user') {
				# code...
				$data_session = array(
				'nama' => $username,
				'status' => "login"
				);
 				
				$this->session->set_userdata($data_session);
				redirect(base_url("user/dashboard"));
			}
		}else{
				$this->session->set_flashdata('fail', 'Username atau Password Salah.');
				redirect(base_url().'home/login');
			}
	}
}
