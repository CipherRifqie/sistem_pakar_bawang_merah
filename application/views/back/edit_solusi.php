    <div class="page">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="javascript:void(0);">Daftar Solusi</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-align-justify"></i>
            </button>
        </nav>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-6 col-sm-12">
                    <div class="card widget_2 big_icon traffic">
                        <div class="body">
                            <div class="col-lg-2">
                                <a class="btn btn-block btn-primary active" href="<?php echo base_url().'admin/daftar_solusi';?>">Kembali</a>
                            </div>
                            <hr>
                            <form action="<?php echo base_url().'admin/edit_solusi_act'; ?>" method="post">
                                <?php foreach($get_where as $gw){?>
                                            <input type="text" class="form-control" hidden="" value="<?php echo $gw->id ?>" aria-label="URL" aria-describedby="basic-addon1" name="id">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Kode solusi</span>
                                                    </div>
                                                    <input type="text" class="form-control" value="<?php echo $gw->kode_solusi ?>" aria-label="URL" aria-describedby="basic-addon1" name="kg" required="">
                                                </div>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Nama solusi</span>
                                                    </div>
                                                    <input type="text" class="form-control" value="<?php echo $gw->nama_solusi ?>" aria-label="Description" name="ng" aria-describedby="basic-addon1" required="">
                                                </div>
                                                <div class="input-group mb-3">
                                                    <input type="submit" class="btn btn-block btn-primary active" value="Edit">
                                                </div>
                                        <?php }?>
                                        </form>
                </div>
            </div>
        </div>
    </div>    
</div>

