        <!-- javascript -->
        <script src="<?php echo base_url().'assets/frontend/js/bootstrap.bundle.min.js'; ?>"></script>
        <script src="<?php echo base_url().'assets/frontend/js/jquery.easing.min.js'; ?>"></script>
        <script src="<?php echo base_url().'assets/frontend/js/scrollspy.min.js'; ?>"></script>
        <script src="<?php echo base_url().'assets/frontend/js/owl.carousel.min.js'; ?>"></script>
        <script src="<?php echo base_url().'assets/frontend/js/owl.init.js'; ?> "></script>
        <script src="<?php echo base_url().'assets/frontend/js/app.js'; ?>"></script>
    </body>
    <script type="text/javascript">
		$(document).ready(function(){
			$('#example').DataTable();
		});
	</script>
</html>