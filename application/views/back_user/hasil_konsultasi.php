    <div class="page">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="javascript:void(0);">Dashboard</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-align-justify"></i>
            </button>
        </nav>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card widget_2 big_icon traffic">
                        <div class="body">
                            <h5>Hasil Konsultasi</h5>
                            <span>Berikut merupakan hasil yang disimpukan : </span><br>
                            <small><p align="justify"><?php echo $this->session->userdata('hasil_perhitungan');?></p></small>
                            <a href="<?php echo base_url();?>/user/konsultasi"><button class="btn btn-primary w-100">Kembali Ke Konsultasi</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>

