    <div class="page">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="javascript:void(0);">Dashboard</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-align-justify"></i>
            </button>
        </nav>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card widget_2 big_icon traffic">
                        <div class="body">
                            <h5>Bawang Merah</h5>
                            <small><p align="justify">Bawang merah adalah salah satu varietas tumbuhan berumbi yang dapat hidup di dataran tinggi. Bawang merah disebut seperti itu karena memiliki warna ungu kemerahan pada kulitnya dan dagingnya. Bawang merah memiliki tekstur yang mirip dengan bawang bombay yaitu berlapis-lapis namun dengan ukuran yang lebih kecil. Bawang merah berbentuk satuan, tidak seperti bawang putih yang umbinya terkumpul dalam satu kulit. Bawang merah memiliki ciri khas berupa bau yang tajam tetapi tidak setajam bawang putih dan aroma gurih serta sedikit pedas. Bawang merah biasanya dipanen beserta daunnya. Daun bawang merah juga dapat digunakan untuk bahan masakan atau taburan. Bawang merah memiliki tekstur yang lebih berair sehingga lebih mudah dihaluskan untuk bumbu masakan. Bawang merah dapat membentuk kulit baru bila disimpan dalam waktu yang lama dalam keadaan terkupas.</p></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>

