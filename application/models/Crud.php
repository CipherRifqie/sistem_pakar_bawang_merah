<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Crud extends CI_Model{

	function tampil_data($table){
		return $this->db->get($table);
	}
	
	function hapus_data($where,$table){
	$this->db->where($where);
	$this->db->delete($table);
	}
	
	function edit_data($where,$table){		
	return $this->db->get_where($table,$where);
	}
	function update_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}	
    
    public function Get_where($table,$acuan)
    {
        $res = $this->db->get_where($table,$acuan);
        return $res->result();
    }

    public function user_where($table,$acuan)
    {
        $res = $this->db->get_where($table,$acuan);
        return $res->result_array();
    }

    public function Get_user($table,$acuan)
    {
        $res = $this->db->get_where($table,$acuan);
        return $res;
    } 

    public function where_num($table, $acuan)
    {
      $res = $this->db->get_where($table,$acuan);
      return $res->num_rows();
    }

    public function num($table)
    {
      $res = $this->db->get($table);
      return $res->num_rows();
    }

     public function like_num($code)
    {
      $this->db->like('url_short', $code);
      $res = $this->db->get('short_url');
      return $res;
    }

    public function Insert($table,$data){
        $res = $this->db->insert($table, $data); // Kode ini digunakan untuk memasukan record baru kedalam sebuah tabel
        return $res; // Kode ini digunakan untuk mengembalikan hasil $res
    }
    public function acak_code($jumlah){
      $permitted_chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
      $code = substr(str_shuffle($permitted_chars), 0, $jumlah);
      return $code;
    }
    function info_client_ip_getenv() {
     $ipaddress = '';
     if (getenv('HTTP_CLIENT_IP'))
         $ipaddress = getenv('HTTP_CLIENT_IP');
     else if(getenv('HTTP_X_FORWARDED_FOR'))
         $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
     else if(getenv('HTTP_X_FORWARDED'))
         $ipaddress = getenv('HTTP_X_FORWARDED');
     else if(getenv('HTTP_FORWARDED_FOR'))
         $ipaddress = getenv('HTTP_FORWARDED_FOR');
     else if(getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
     else if(getenv('REMOTE_ADDR'))
         $ipaddress = getenv('REMOTE_ADDR');
     else
         $ipaddress = 'UNKNOWN';

     return $ipaddress; 
    }
    public function hitung($table,$acuan)
    {
        $this->db->count('url');
        $this->db->where($acuan);
        $res = $this->db->get($table);
        return $res;
    }
}
?>