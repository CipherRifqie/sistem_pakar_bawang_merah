-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 03 Okt 2019 pada 11.58
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shortener_url`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `short_url`
--

CREATE TABLE `short_url` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `url_asli` text NOT NULL,
  `url_short` text NOT NULL,
  `code` varchar(10) NOT NULL,
  `slug` varchar(10) NOT NULL,
  `description` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `short_url`
--

INSERT INTO `short_url` (`id`, `username`, `url_asli`, `url_short`, `code`, `slug`, `description`, `status`) VALUES
(1, '0686.89.2017.0005', 'https://dinus.ac.id/getinfo/3725/Jadwal-Pertemuan-Seminar-Proposal', 'localhost/home/u/LxWb2', 'LxWb2', '-', 'Seminar Proposal SI-S1', 1),
(2, '0686.89.2017.0006', 'https://dinus.ac.id/getinfo/3721/Pengumuman-Kelas-Kosong-Matakuliah-BAHASA-INGGRIS-TERAPAN-DALAM-PRESENTASI', 'localhost/home/u/ingKosong', 'ingKosong', 'ingKosong', 'kelas kosong', 1),
(3, '0686.89.2017.0006', 'https://dinus.ac.id/getinfo/3722/JADWAL-BIMBINGAN-TA-TI-S1-TERSTRUKTUR-CLASICAL-TIAP-MINGGU', 'localhost/home/u/taTI-S1', 'taTI-S1', 'taTI-S1', 'bimbingan Tugas Akhir TI-S1', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `statistic`
--

CREATE TABLE `statistic` (
  `id` int(11) NOT NULL,
  `url` varchar(50) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `statistic`
--

INSERT INTO `statistic` (`id`, `url`, `ip`, `datetime`) VALUES
(1, 'localhost/home/u/LxWb2', '::1', '2019-10-01 16:30:26'),
(2, 'localhost/home/u/ingKosong', '::1', '2019-10-01 16:49:51');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user`
--

CREATE TABLE `tbl_user` (
  `tbl_id` int(11) NOT NULL,
  `npp` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` int(11) NOT NULL,
  `last_login` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_user`
--

INSERT INTO `tbl_user` (`tbl_id`, `npp`, `password`, `level`, `last_login`) VALUES
(1, '0686.89.2017.0005', '21232f297a57a5a743894a0e4a801fc3', 1, '2019-10-01 16:27:50'),
(2, '0686.89.2017.0006', 'ee11cbb19052e40b07aac0ca060c23ee', 0, '2019-10-01 16:40:57');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `short_url`
--
ALTER TABLE `short_url`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `statistic`
--
ALTER TABLE `statistic`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`tbl_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `short_url`
--
ALTER TABLE `short_url`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `statistic`
--
ALTER TABLE `statistic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `tbl_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
