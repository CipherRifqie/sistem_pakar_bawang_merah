-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 03 Nov 2019 pada 14.51
-- Versi server: 10.4.8-MariaDB
-- Versi PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sistempakarbawang`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `konsultasi`
--

CREATE TABLE `konsultasi` (
  `id` int(10) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `penyakit` varchar(100) DEFAULT NULL,
  `gejala` varchar(250) DEFAULT NULL,
  `solusi` varchar(500) DEFAULT NULL,
  `hasilkonsultasi` varchar(20) DEFAULT NULL,
  `keterangan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `konsultasi`
--

INSERT INTO `konsultasi` (`id`, `nama`, `penyakit`, `gejala`, `solusi`, `hasilkonsultasi`, `keterangan`) VALUES
(1, 'keepsmile', 'Ulat Grayak , Lalat Penggorok Daun', 'Terapat bercak putih memanjang menjadi seperti membran dan layu pada daun , Terlihat pada daun berupa bercak mengilap dan luka bekas gigitan yang berbentuk bintik-bintik berwarna putih, lalu berubah menjadi abu-abu perak dan mengering', 'Mengumpulkan kelompok telur dan ulat bawang lalu dibutit (dimasukkan dalam kantong plastik dan diikat), terutama pada saat tanaman bawang merah berumur 7-35 hari kemudian dimusnahkan. , Pemasangan perangkap feromonoid seks dipasang sebanyak 40 buah/ha untuk menangkap ngengat S. Exigua segera setelah tanaman bawang merah ditanam.', '24.285714285714', 'Tingkat Serangan Ringan'),
(2, 'keepsmile', 'Ulat Grayak , Hama Bodas', 'Terapat bercak putih memanjang menjadi seperti membran dan layu pada daun , Daun bawang terlihat menerawang tembus cahaya atau terlihat bercak putih transparan dan daunya terkulai', 'Mengumpulkan kelompok telur dan ulat bawang lalu dibutit (dimasukkan dalam kantong plastik dan diikat), terutama pada saat tanaman bawang merah berumur 7-35 hari kemudian dimusnahkan. , Memasang lampu perangkap (neon 7-10 watt jumlah sekitar 25-30 buah/ha), mulai dari 1 minggu sebelum tanam sampai menjelang panen (Â± 60 hari), dari pukul 18.00-06.00. Ketinggian lampu 10-15 cm (dari permukaan tempat air s.d. pucuk tanaman) sedangkan mulut bak perangkap tidak boleh lebih dari 40 cm diatas pucuk ta', '42', 'Tingkat Serangan Sedang'),
(3, 'keepsmile', 'Ulat Grayak , Hama Bodas , Lalat Penggorok Daun', 'Terapat bercak putih memanjang menjadi seperti membran dan layu pada daun , Daun bawang terlihat menerawang tembus cahaya atau terlihat bercak putih transparan dan daunya terkulai , Terlihat pada daun berupa bercak mengilap dan luka bekas gigitan yan', 'Mengumpulkan kelompok telur dan ulat bawang lalu dibutit (dimasukkan dalam kantong plastik dan diikat), terutama pada saat tanaman bawang merah berumur 7-35 hari kemudian dimusnahkan. , Memasang lampu perangkap (neon 7-10 watt jumlah sekitar 25-30 buah/ha), mulai dari 1 minggu sebelum tanam sampai menjelang panen (Â± 60 hari), dari pukul 18.00-06.00. Ketinggian lampu 10-15 cm (dari permukaan tempat air s.d. pucuk tanaman) sedangkan mulut bak perangkap tidak boleh lebih dari 40 cm diatas pucuk ta', '24.375', 'Tingkat Serangan Ringan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_gejala`
--

CREATE TABLE `tb_gejala` (
  `id` int(10) NOT NULL,
  `kode_gejala` varchar(10) NOT NULL,
  `nama_gejala` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_gejala`
--

INSERT INTO `tb_gejala` (`id`, `kode_gejala`, `nama_gejala`) VALUES
(1, 'G01', 'Terapat bercak putih memanjang menjadi seperti membran dan layu pada daun'),
(2, 'G02', 'Daun bawang terlihat menerawang tembus cahaya atau terlihat bercak putih transparan dan daunya terkulai'),
(3, 'G03', 'Terlihat pada daun berupa bercak mengilap dan luka bekas gigitan yang berbentuk bintik-bintik berwarna putih, lalu berubah menjadi abu-abu perak dan mengering'),
(6, 'G04', 'Terjadi serangan hewan yang dimulai dari ujung-ujung daun yang masih muda, perkembangan dan penyebaran hama ini cepat sekali'),
(7, 'G05', 'Hampir seluruh helaian daun penuh dengan korokan sehingga menjadi kering dan berwarna coklat seperti terbakar'),
(9, 'G06', 'Terdapat larva penggorok pada umbi bawang'),
(10, 'G07', 'Terdapat hewan yang tinggal di bawah permukaan tanah, menyerupai jangkrikdengan panjang sekitar 3 cm dan berwarna merah tua'),
(11, 'G08', 'Terdapat terowongan tanah yang berbentuk horizontal dan lebih dekat dengan permukaan tanah'),
(12, 'G09', 'Dari kejauhan, daun terlihat abu-abu'),
(13, 'G10', 'Umbi bawang merah menjadi keropos'),
(14, 'G11', 'Jika umbi dibelah ditemukan larva atau kotorannya'),
(15, 'G12', 'Terdapat bercak berukuran kecil, melekuk ke dalam, berwarna putih dengan pusat yang berwarna ungu (kelabu) pada daun'),
(16, 'G13', 'Jika cuaca lembab, bercak berkembang hingga menyerupai cincin dengan bagian tengah yang berwarna ungu dengan tepi yang kemerahan dikelilingi warna kuning yang dapat meluas ke bagian atas maupun bawah bercak'),
(17, 'G14', 'Ujung daun membusuk dan mengering sehingga menjadi patah'),
(18, 'G15', 'Umbi membusuk dan berair yang dimulai dari bagian leher kemudian mengering dan berwarna lebih gelap '),
(19, 'G16', 'Terdapat bercak putih pada daun selanjutnya terbentuk lekukan ke dalam, berlubang dan patah karena terkulai tepat pada bercak'),
(20, 'G17', 'Pada hamparan tanaman terlihat gejala botak-botak pada beberapa tempat'),
(21, 'G18', 'pada saatt tanaman mulai membentuk umbi lapis, didekat ujung daun timbul bercak hijau pucat'),
(22, 'G19', 'Terdapat daun yang mati berwarna putih diliputi oleh kapang hitam'),
(23, 'G20', 'Pada waktu cuaca lembab pada permukaan daun berkembang kapang (mould jamur) yang berwarna putih lembayung atau ungu'),
(24, 'G20', 'Umbi membusuk sehingga lama-kelamaan tanaman mati'),
(25, 'G22', 'tanaman menjadi cepat layu, akar tanaman busuk, tanaman terkulai seperti akan roboh, dan di dasar umbi lapis terlihat koloni jamur berwarna putih'),
(26, 'G23', 'Daun bawang merah menguning dan terpelintir layu (moler) serta tanaman mudah tercabut karena pertumbuhan akar terganggu dan membusuk'),
(27, 'G24', 'Bercak-bercak berwarna putih kekuning-kuningan, tumbuh sangat banyak dan cepat sesuai dengan arah bertiupnya angin di awal pertanaman');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_penyakit`
--

CREATE TABLE `tb_penyakit` (
  `id` int(10) NOT NULL,
  `kode_penyakit` varchar(10) NOT NULL,
  `nama_penyakit` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_penyakit`
--

INSERT INTO `tb_penyakit` (`id`, `kode_penyakit`, `nama_penyakit`) VALUES
(1, 'PO1', 'Ulat Grayak'),
(2, 'P02', 'Hama Bodas'),
(3, 'P03', 'Lalat Penggorok Daun'),
(4, 'P04', 'Orong-orong (Anjing Tanah)'),
(5, 'P05', 'Tungau'),
(6, 'P06', 'Ngengat Gudang'),
(7, 'P07', 'Bercak Ungu'),
(8, 'P08', 'Antraknose'),
(9, 'P09', 'Embun Bulu'),
(10, 'P10', 'Layu Fusarium (Moler)'),
(11, 'P11', 'Nglumpruk (Leumpeuh)'),
(14, 'S01', 'Mengumpulkan kelompok telur dan ulat bawang lalu d'),
(15, 'P025', 'Sakit Hati');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_solusi`
--

CREATE TABLE `tb_solusi` (
  `id` int(11) NOT NULL,
  `kode_solusi` varchar(10) NOT NULL,
  `nama_solusi` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_solusi`
--

INSERT INTO `tb_solusi` (`id`, `kode_solusi`, `nama_solusi`) VALUES
(1, 'S01', 'Mengumpulkan kelompok telur dan ulat bawang lalu dibutit (dimasukkan dalam kantong plastik dan diikat), terutama pada saat tanaman bawang merah berumur 7-35 hari kemudian dimusnahkan.'),
(2, 'S01', 'Memasang lampu perangkap (neon 7-10 watt jumlah sekitar 25-30 buah/ha), mulai dari 1 minggu sebelum tanam sampai menjelang panen (Â± 60 hari), dari pukul 18.00-06.00. Ketinggian lampu 10-15 cm (dari permukaan tempat air s.d. pucuk tanaman) sedangkan mulut bak perangkap tidak boleh lebih dari 40 cm diatas pucuk tanaman. Jarak antar lampu 20 m x 15 m.'),
(3, 'S01', 'Pemasangan perangkap feromonoid seks dipasang sebanyak 40 buah/ha untuk menangkap ngengat S. Exigua segera setelah tanaman bawang merah ditanam.'),
(4, 'S02', 'Memangkas bagian daun yang terserang.'),
(5, 'S02', 'Penggunaan insektisida fosfororganik, seperti Bayrusil 250 EC yang mengandung bahan aktif kuinalfos, Mesurol 50 WP yang mengandung bahan aktif merkaptodimetur, ataupun Azodrin 15 WSC dan Nuvacron 20 SCW yang mengandung bahan aktif monokotofos.'),
(7, 'S03', 'Pergiliran tanaman; lalat L. chinensis baru diketahui hanya menyerang tanaman golongan bawang, maka bila disuatu wilayah terjadi serangan berat, sebaiknya satu musim berikutnya tidak menanam tanaman  golongan bawang.'),
(8, 'S03', 'Penggunaan mulsa plastik; mulsa plastic berwarna perak dipasang sebelum tanam, lalu diberi lubang disetiap titik jarak tanam dengan garis tengah lubang yang cukup untuk berkembangnya tanaman bawang merah sampai panen akan mematikan larva yang jatuh dari daun.'),
(9, 'S03', 'Pengambilan daun yang menunjukkan gejala korokan dipotong dan dibutit lalu dimusnahkan.'),
(10, 'S03', 'Pemasangan kain kelambu'),
(11, 'S03', 'Perangkap lampu neon (TL 10 watt) dengan waktu nyala mulai pukul 18.00-24.00 paling efisien dan efektif untuk menangkap imago.'),
(12, 'S04', 'Penggunaan pupuk kandang yang matang dapat mengurangi serangan Gryllotalpa sp.'),
(13, 'S04', 'Menjaga kebersihan kebun (sanitasi) dapat mengurangi serangan Gryllotalpa sp.'),
(14, 'S04', 'Pemasangan umpan beracun yang terdiri dari 10 kg dedak dicampur dengan 100 ml insektisida yang dianjurkan kemudian campuran tersebut diaduk secara merata dan disebar diatas bedengan pertanaman pada senja hari.'),
(15, 'S05', 'Penggunaan akarisida, seperti Meotrin 50 EC yang mengandung bahan aktif fenpropatrin atau Roxion 40 EC yang mengandung bahan aktif dimetoat. Konsentrasinya 2 ml/l air. Penyemprotan dimulai sejak tanaman berumur 9 minggu hingga 2 minggu sebelum panen dengan selang waktu seminggu sekali.'),
(16, 'S06', 'Menyemprotkan pestisida kedalam ruangan tertutup atau kedap udara untuk beberapa waktu dalam dosis dan konsentrasi yang dapat mematikan hama.'),
(17, 'S07', 'menanam bawang di lahan yang mempunyai drainasi baik dan dengan mengadakan pergiliran tananman (rotasi)'),
(18, 'S07', 'Penyiraman setelah turun hujan dikatakan dapat mengurangi serangan, ini disebabkan karena penyiraman dapat mencuci konidium yang menempel pada daun bersama percikan air tanah'),
(19, 'S07', 'Jika diperlukan,penyakit dapat dikendalikan dengan penyemprotan fungisida.'),
(20, 'S025', 'Sakit Hati');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(10) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `telepon` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id`, `nama`, `alamat`, `telepon`, `email`, `username`, `password`, `level`) VALUES
(1, 'code boys', 'Desa Pakuncen RT 04 RW 01, Kec. Bobotsari', '', 'ust.crew404@gmail.com', 'keepsmile', '229b5830baa3d60f5312680fca331ceb', 'user'),
(2, 'Muhamad Aziz Setiyalaksono', 'Desa Pakuncen RT 04 RW 01, Kec. Bobotsari', '', 'revanthine77@gmail.com', 'Keepgoing', '229b5830baa3d60f5312680fca331ceb', 'user'),
(3, 'Dede Untukng', 'Brebes', '082136830853', 'alif.mgch@gmail.com', 'administrator', '0192023a7bbd73250516f069df18b500', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `konsultasi`
--
ALTER TABLE `konsultasi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_gejala`
--
ALTER TABLE `tb_gejala`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_penyakit`
--
ALTER TABLE `tb_penyakit`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_solusi`
--
ALTER TABLE `tb_solusi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `konsultasi`
--
ALTER TABLE `konsultasi`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tb_gejala`
--
ALTER TABLE `tb_gejala`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `tb_penyakit`
--
ALTER TABLE `tb_penyakit`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `tb_solusi`
--
ALTER TABLE `tb_solusi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
